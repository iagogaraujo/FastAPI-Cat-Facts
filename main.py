from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
import requests

app = FastAPI()

templates = Jinja2Templates(directory="templates")

@app.get("/")
def serve_home(request: Request):
  facts = requests.get("https://cat-fact.herokuapp.com/facts").json()

  return templates.TemplateResponse(
      "index.html",
      context={"request": request, "facts": facts},
  )
